#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;

	//Функция приема 
	
	bool receiveSome (int from, char* data, size_t size)
	{	
		int received = 0;
		int b_received = 0;
				
		size_t b_size;
		uint64_t max_count;
		b_size=0;

		cout << "Осталось принять: " << b_received << endl;
		cout << "Принято байт: " << size << endl;

		while(b_received != size)
		{
			received = recv(from, data, size - b_received, 0);
			b_received += received;
			cout << "Всего получено: " << b_received << endl;
			cout<<"Осталось принять : "<< size - b_received <<endl;
			
			}
			
			cout << "Сообщение принято!" << endl;
			return true;

		}
		// функция отправки
	bool sendSome (int to,  char* data, size_t size)
	{
		int b_send = 0;
		while (b_send != size)
		{
			b_send = send(to, data, size, 0);
			cout << "Отправленно: " << b_send << " bytes" << endl;
		}
		return true;
	}
	
int main()
{
		cout << "!Client!/n" << endl;
		auto channel = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		char ip_server[15];
		short port_server;
		sockaddr_in x;
		x.sin_family = AF_INET;
		//int count;
		//char file_name[255];
		//uint64_t max_count;
		char* data;
		//Запрашиваем необходимые данные для подключения
		cout << "Enter IP server:/n"<<endl;
		cin >> ip_server;
		cout << "Enter port server:/n"<<endl;
		cin >> port_server;
		
		
		
		//Установка соединения с сервером
		x.sin_addr.s_addr = inet_addr(ip_server);
		x.sin_port = htons(port_server);
		int result = connect(channel,(const sockaddr*)(&x),sizeof (x));

		/*
		//Отправка данных
		string str;
		while (true)
		{
			cout << "Enter file name:/n"<<endl;
			cin >> str;
			vector <char> file_name(str.begin(), str.end());
			uint64_t sizeForSending = file_name.size();
			cout << "Str size is: " << sizeForSending << endl;

			//int bytes_send = send(channel, data, count, 0);
			if (sendSome(channel, (char*)&sizeForSending, sizeof(sizeForSending)))
			{
				
				break;
			}
			
			receiveSome (channel, (char*) &max_count, sizeof(max_count));
		receiveSome (channel, char* data, max_count);
		*/
		bool connected = true;
		int key = 0;
		string str;
		while (connected)
		{
			key = 0;
			cout << "\nВведите номер операции\n"
				<< "1 - Отправить сообщение"
				<< "2 - Принять "
				<< "3 - Принудительный выход";
			cin >> key;
			cout << endl;
			cin.get();

			if (key==1)
			{

				cout << "Введите имя файла или /quit: ";
				getline(cin, str);
				cin.get();

				//Формируем тип сообщения.
				char msgType = '\0';
				if (strcmp(str.data(), "/q") == 0)
				{
					msgType = 'Q';
				}
				else
				{
					msgType = 'R';
				}

				vector<char> fileName(str.begin(), str.end());

				//size_t sizeforsending = data.size();
				uint64_t sz = fileName.size()+1+sizeof(msgType);
				cout << "Размер: " << sz << endl;

				//Отсылаем размер сообщения.
				if (sendSome(channel, (char*)&sz, sizeof(sz)))
				{
					//Отсылаем тип сообщения.
					if (sendSome(channel, &msgType, sizeof(msgType)))
					{
						if (msgType == 'Q')
						{
							close(channel);
							return 0;
						}
						//Отсылаем само сообщение
						if (sendSome(channel, fileName.data(), fileName.size()+1))
						{
							cout << "Отправлено\n";
						}
						else
						{
							cout << "Сообщение плохо отправлено\n";
						}
					}
					else
					{
						cout << "Неправильно определен тип\n";
					}
				}
				else
				{
					cout << "Некорректный размер\n";
				}
				cin.get();
			//	break;
			}

			else if(key==2)
			{
				uint64_t msgSize = 0;

				//Принимает размер
				if (receiveSome(channel, (char*)&msgSize, sizeof(msgSize)))
				{
					cout << "Размер сообщения: " << msgSize << endl;
					char msgType = '\0';

					if (receiveSome(channel, &msgType, sizeof(msgType)))
					{
						cout << "Тип сообщения: " << msgType << endl;
						vector<char> msg;
						msg.resize(msgSize + 1, '\0');

						if (receiveSome(channel, msg.data(), (msgSize)-1))
						{
							if (msgType != 'E')
							{
								char key = '\0';
								//cout << "Сохранить (1) или распечатать (2) файл?\n";
								//cin >> key;
								//if (key==1)
								//{
									str="x1";
									cout << "Имя файла копии: " << str << endl;
									ofstream output(str, ios::binary);
									output.write(msg.data(), msg.size()-1);
									output.close();
								//}
								//else if (key==2)
								//{
									cout << msg.data() << endl;
									cin.get();
									
									//break;
								//}
							//}
							//else
							//{
							//	cout << msg.data() << endl;
							//	//cin.get();
								//break;
							}
						}
					}
				}
				//break;
			}

			else if (key==3)
			{
				shutdown(channel, SHUT_RDWR);
				return 0;
			}
			/*default:
				cout << "Wrong key!\n";
				break;*/
			}

	
		
    return 0;
}
